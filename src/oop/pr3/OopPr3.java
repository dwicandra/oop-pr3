/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop.pr3;

import java.util.ArrayList;
/**
 *
 * @author Dwicandra
 */
public class OopPr3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Inisialisasi untuk constructor tanpa parameter
        Bilangan bil1 = new Bilangan();
        
        // Inisialisasi untuk constructor dengan parameter
        Bilangan bil2 = new Bilangan(7);
        
        bil1.addBilangan(2);
        bil1.addBilangan(3);
        bil1.addBilangan(5);
        bil1.addBilangan(7);
        bil1.addBilangan(9);
        
        bil2.addBilangan(1);
        bil2.addBilangan(11);
        bil2.addBilangan(257);
        
        System.out.println("Rata-rata Bilangan");
        System.out.println("==================");
        System.out.print("Bilangan: ");
        bil1.tampilkanBilangan();
        System.out.println("Rata-rata = " + bil1.rataRata());
        
        System.out.println("");
        System.out.println("Rata-rata Bilangan");
        System.out.println("==================");
        System.out.print("Bilangan: ");
        bil2.tampilkanBilangan();
        System.out.println("Rata-rata = " + bil2.rataRata());
        
    }
    
}

class Bilangan {
    private ArrayList<Integer> bilangan = new ArrayList<>(); 
    private Double rataRata = 0.0;
    Bilangan(){}
    Bilangan(int i){
        addBilangan(i);
    }
    
    /* Prosedur untuk menambah bilangan ke array */
    public void addBilangan(int i){
        bilangan.add(i);
        rataRata += i; // Implicit type casting (Double dikalikan integer = double)
        rataRata /= bilangan.size(); //Rata-rata langsung dihitung
    }
    
    /* Prosedur untuk mengembalikan nilai dari property "rataRata" */
    public double rataRata() {
        return rataRata;
    }
    
    public void tampilkanBilangan() {
        for(int i=0; i<bilangan.size(); i++) {
            if(i>0){
                System.out.print(", ");
            }
            
            System.out.print(bilangan.get(i));
        }
        System.out.println(".");
    }
}